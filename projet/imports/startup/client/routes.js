import { FlowRouter } from 'meteor/kadira:flow-router';
import { BlazeLayout } from 'meteor/kadira:blaze-layout';


import '../../ui/layouts/mainLayout.html';

//List of Layout
import '../../ui/layouts/setPseudo/setPseudo.js';
import '../../ui/layouts/join/join.js';
import '../../ui/layouts/ownerRoom/ownerRoom.js';
import '../../ui/layouts/role/role.js';

FlowRouter.triggers.enter([function(context,redirect){
    if(localStorage.getItem('pseudo') == null || localStorage.getItem('id') == null){
        FlowRouter.go('setPseudo');
    }
}]);

FlowRouter.notFound = {
    action: function() {
        if(localStorage.getItem('pseudo') != null && localStorage.getItem('id') != null){
		    FlowRouter.go('join');
		}else{
            FlowRouter.go('setPseudo');
        }
    }
};

FlowRouter.route('/',{
	name: 'setPseudo',
	action(){
		if(localStorage.getItem('pseudo') != null && localStorage.getItem('id') != null){
		    FlowRouter.go('join');
		}
		BlazeLayout.render('mainLayout', {main: 'setPseudo'});
	}
});

FlowRouter.route('/join',{
	name: 'join',
	action(){
		BlazeLayout.render('mainLayout', {main: 'join'});
	}
});

FlowRouter.route('/ownerRoom/:id',{
	name: 'ownerRoom',
	action(){
		BlazeLayout.render('mainLayout', {main: 'ownerRoom'});
	}
});

FlowRouter.route('/role/:id',{
	name: 'role',
	action(){
		BlazeLayout.render('mainLayout', {main: 'role'});
	}
});