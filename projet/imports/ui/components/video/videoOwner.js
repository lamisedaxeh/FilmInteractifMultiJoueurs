import Plyr from 'plyr';
import { Rooms } from '../../../api/rooms.js';
import { Template } from 'meteor/templating';
import './videoOwner.html';
import '../../style/plyr.css';
// this is equivalent to the standard node require:


var player;
Template.videoOwner.onCreated(function (){
	var template = this;
	//Sub on template, if template was remove -> sub was remove to
	template.autorun(function (){
		const id = FlowRouter.getParam('id');
		template.subscribe('oneRoom', id);
	});
});


Template.videoOwner.events({
	'click .button-choix'(evt){
		if(document.getElementsByClassName("button-selected").length != 0){
			let list = document.getElementsByClassName("button-selected");
			for ( let variable of list) {
				variable.classList.remove("button-selected");
			}
		}
		evt.currentTarget.classList.add("button-selected");
		
		const id = FlowRouter.getParam('id');
		var userId = localStorage.getItem("id");
		var userName = localStorage.getItem("pseudo");
		var role =  localStorage.getItem(id);
		var choix = evt.currentTarget.id;
		var actualNode = Rooms.findOne( { _id : id } ).node;
		
		
		Meteor.call('rooms.insertChoix', id, userId, userName, role, choix, actualNode);
	},
});


Template.videoPlayer.helpers({
	isOwner(){
		const id = FlowRouter.getParam('id');
		room = Rooms.findOne( { _id : id } );
		return localStorage.getItem("Id") === room.idAdmin;
	},
	playing: function(){
		const id = FlowRouter.getParam('id');
		room = Rooms.findOne( { _id : id } );

		// console.log(player + Rooms.findOne(id).playing);

		//Syncro play pause
		//this made erros in blaze
		actualNode = room.node;
			
		let histoireJson = require('./histoire.json');
		var nodeObj = histoireJson.nodes[actualNode];
	
		if(player){
			if(player.source.substring(player.source.length - 11) != nodeObj.videoId){
				player.source = {
					type: 'video',
					sources: [
						{
							src: nodeObj.videoId,
							provider: 'youtube',
						},
					],
				};
			}
			if(player.playing == false && room.playing == true){
				player.play();
			}else if(player.playing == true && room.playing == false){
				player.pause();
			}if(player.currentTime - room.currentTime > 1 ||player.currentTime-room.currentTime < -1 ){
			    console.log(player.currentTime - room.currentTime);
			    player.currentTime = room.currentTime;
	    	}
		}
		return room.playing;
	},
	rep1: function(){
		const id = FlowRouter.getParam('id');
		actualNode = Rooms.findOne( { _id : id } ).node;
		
		var role =  localStorage.getItem(id);
		let histoireJson = require('./histoire.json');
		var nodeObj = histoireJson.nodes[actualNode];
		if(Object.keys(nodeObj.roles).length ==0){
			//document.getElementById("choix").setAttribute("style","display:none;");
			return "Good Game !";
		}
		return nodeObj.roles[role].choix.A;		
	},
	rep2: function(){
		const id = FlowRouter.getParam('id');
		actualNode = Rooms.findOne( { _id : id } ).node;
		
		var role =  localStorage.getItem(id);
		let histoireJson = require('./histoire.json');
		var nodeObj = histoireJson.nodes[actualNode];
		if(Object.keys(nodeObj.roles).length ===0){
			//document.getElementById("choix").setAttribute("style","display:none;");
			return "Good Game !";
		}
		return nodeObj.roles[role].choix.B;		
	},
	rep3: function(){
		const id = FlowRouter.getParam('id');
		actualNode = Rooms.findOne( { _id : id } ).node;
		
		var role =  localStorage.getItem(id);
		let histoireJson = require('./histoire.json');
		var nodeObj = histoireJson.nodes[actualNode];
		console.log(Object.keys(nodeObj.roles).length );
		if(Object.keys(nodeObj.roles).length ===0){
			//document.getElementById("choix").setAttribute("style","display:none;");
			return "Good Game !";
		}
		return nodeObj.roles[role].choix.C;		
	},
	rep4: function(){
		const id = FlowRouter.getParam('id');
		actualNode = Rooms.findOne( { _id : id } ).node;
		
		var role =  localStorage.getItem(id);
		let histoireJson = require('./histoire.json');
		var nodeObj = histoireJson.nodes[actualNode];
		if(Object.keys(nodeObj.roles).length ===0){
			//document.getElementById("choix").setAttribute("style","display:none;");
			return "Good Game !";
		}
		return nodeObj.roles[role].choix.D;		
	},

	
	
});


Template.videoPlayer.onRendered(function () {
	const id = FlowRouter.getParam('id');
	//Setup du player vidéo
	//currentTime = Rooms.findOne( { _id : id } ).currentTime;
	room = Rooms.findOne( { _id : id } );
	currentTime = 0;
	if(localStorage.getItem("id") && localStorage.getItem("id") === room.idAdmin){
	    player = new Plyr('#player', {
			muted:true,
			autoplay: true,
			hideControls: true,
			controls: ['play-large', 'play', 'progress', 'current-time', 'mute', 'volume',  'captions', 'settings', 'pip', 'airplay', 'fullscreen'],
			settings: ['captions','quality'],
			storage: {enabled: false,key: "playerData"},
	    });  
	}else{
        player = new Plyr('#player', {
			muted:true,
			autoplay: true,
			hideControls: true,
			clickToPlay: false,
			controls: ['play-large', 'progress', 'current-time', 'mute', 'volume',  'captions', 'settings', 'pip', 'airplay', 'fullscreen'],
			settings: ['captions','quality'],
			storage: {enabled: false,key: "playerData"},
	    });
	}

	/*   player.on("ready", function() {
	//        this.seek(seconds);
	   player.currentTime = currentTime;
	   player.muted = true;
    });
    */
	
	player.on('ended',event => {
		var result = Rooms.findOne({ _id : id });
		console.log(result);
		
		//RECUPERATION DES VOTES
		var r1 = [0,0,0,0];
		var r2 = [0,0,0,0];
		var r3 = [0,0,0,0];
		var r4 = [0,0,0,0];
		//FUNCTION FIND NEXT VIDEO
		for(user in result.users)
		{
			user = result.users[user];
			switch(user.role){
				case "r1":
					switch(user.choix.choix){
						case "rep1":
							r1[0]++;
							break;
						case "rep2":
							r1[1]++;
							break;
						case "rep3":
							r1[2]++;
							break;
						case "rep4":
							r1[3]++;
							break;
					}
					break;
				case "r2":
					switch(user.choix.choix){
						case "rep1":
							r2[0]++;
							break;
						case "rep2":
							r2[1]++;
							break;
						case "rep3":
							r2[2]++;
							break;
						case "rep4":
							r2[3]++;
							break;
					}
					break;
				case "r3":
					switch(user.choix.choix){
						case "rep1":
							r3[0]++;
							break;
						case "rep2":
							r3[1]++;
							break;
						case "rep3":
							r3[2]++;
							break;
						case "rep4":
							r3[3]++;
							break;
					}
					break;
				case "r4":
					switch(user.choix.choix){
						case "rep1":
							r4[0]++;
							break;
						case "rep2":
							r4[1]++;
							break;
						case "rep3":
							r4[2]++;
							break;
						case "rep4":
							r4[3]++;
							break;
					}
					break;
			}
		} 
		
		//Number to letter
		function number2Letter(n) {
			if(n==0)
				return "A";
			if(n==1)
				return "B";
			if(n==2)
				return "C";
			if(n==3)
				return "D";
		}
		
		//INTERPRETE LES VOTES POUR OBTENIR LE CHOIX DU PERSONAGE FINAL
		function getChoix(r){
			var idMaxR = [];
			var max = -1;
			//Trouve les cases avec le MAX
			for(var i in r){
				if(r[i] != 0){
					if(r[i] === max)
					{
						idMaxR.push(i);
					}else if(r[i] > max ){
						max = r[i];
						idMaxR = [i];
					}
				}
			}
			if(idMaxR.length === 1){
				//CHOIX fixe
				return number2Letter(idMaxR[0]);
			}else if(idMaxR.length > 1){
				//Choix RAMDOM parmis les selections
				//choisis au pif une case du tableau idMaxR
				return number2Letter( idMaxR[Math.floor(Math.random() * idMaxR.length)] );
			}else if(idMaxR.length < 1) {
				//Choix RANDOM
				//Génére un choix entre 0 et 3
				//TODO faire un choix random dans les choix existant
				return number2Letter(0/*Math.floor(Math.random() * 4)*/);
			}
		}
		//CHOIX DES PERSONAGE
		var choix = getChoix(r1)+getChoix(r2)+getChoix(r3)+getChoix(r4);
		console.log("res : " + choix);
		
		actualNode = result.node;
		
		let histoireJson = require('./histoire.json');
		var nodeObj = histoireJson.nodes[actualNode];
		console.log(nodeObj);
		
		var nextNode = histoireJson.nodes[nodeObj.link[choix]];
		
		player.source = {
			type: 'video',
			sources: [
				{
					src: nextNode.videoId,
					provider: 'youtube',
				},
			],
		};
		
		
		Meteor.call('rooms.setNode', id, nodeObj.link[choix]);
		/*if(nextNode.link.== 0){
			console.log("ZEORRORORRO")
		}*/
		
		//TODO MAJ DE LA BDD
		//TODO MAJ DES BUTTON !!!
	});

	//FUNCTION PROGRESS BAR TIMER
	player.on('timeupdate', event => {
		tempsTimer = 10;
		if(player.duration - player.currentTime < tempsTimer && player.duration - player.currentTime != 0){
			//AFFICHER LES CHOIX + RESIZE LE PLAYER
			document.getElementById("div-player").classList.add("player-avec-choix");
			document.getElementById("div-player").classList.remove("player-sans-choix");
			document.getElementById("choix").classList.remove("hide");
			
			//VIDE LA PROGRESS BAR
			document.getElementsByClassName("progress-bar")[0].setAttribute("aria-valuenow", ((player.duration - player.currentTime)/tempsTimer)*100 );
			document.getElementsByClassName("progress-bar")[0].setAttribute("style","width:"+((player.duration - player.currentTime)/tempsTimer)*100+"%");
		}else{
			//CACHE LES CHOIX + RESIZE LE PLAYER
			document.getElementById("div-player").classList.add("player-sans-choix");
			document.getElementById("div-player").classList.remove("player-avec-choix");
			document.getElementById("choix").classList.add("hide");
				
			//REMPLIS LA PROGRESS BAR
			document.getElementsByClassName("progress-bar")[0].setAttribute("style","width:100%");
		}
	 });
	
	if(localStorage.getItem("id") && localStorage.getItem("id") === room.idAdmin){
	    player.on('play', event => {
		    Meteor.call('rooms.setStatusPlaying', id, event.detail.plyr.playing);
	    });
	    player.on('pause', event => {
		    Meteor.call('rooms.setStatusPlaying', id, event.detail.plyr.playing);
	    });
		
	    //Check each second en cas de PB avec l'event timeUpdate
	    var t = setInterval(checkTime,1000);
		
	    function checkTime(){
			//console.log("timeupdate" + player.currentTime);
			Meteor.call('rooms.setStatusTimePlayer', id , player.currentTime);
	    }    
	    player.on('seeking', event => {
			Meteor.call('rooms.setStatusTimePlayer', id, player.currentTime);
	    });
	    player.on('seeked', event => {
			Meteor.call('rooms.setStatusTimePlayer', id, player.currentTime);
	    });
	    /*player.on('timeupdate', event => {
	    //console.log("timeupdate" + event.detail.plyr.currentTime);
	    Meteor.call('rooms.setStatusTimePlayer', id , event.detail.plyr.currentTime);
	    });*/
	}else{
	    player.on('play', event => {
		    room = Rooms.findOne( { _id : id } );
		    if(room.playing == false){
				player.pause();
				player.currentTime = room.currentTime;
			}
		    
	    });
	}
});
