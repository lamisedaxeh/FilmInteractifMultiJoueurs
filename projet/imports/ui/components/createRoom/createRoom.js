import { Rooms } from '../../../api/rooms.js';
import { Random } from 'meteor/random';
import './createRoom.html';

//Get all MSG
Template.createRoom.helpers({
	roooms: function(){
		return Rooms.find();
	},
});



Template.createRoom.events({
	'click .radio-button'(evt){
		if(document.getElementsByClassName("radio-button-selected").length != 0){
			let list = document.getElementsByClassName("radio-button-selected");
			for ( let variable of list) {
				variable.classList.remove("radio-button-selected");
			}
		}
		evt.currentTarget.classList.add("radio-button-selected");
	},
	//Save Room
	'submit .createRoom'(event){
		event.preventDefault();
		idAdmin = localStorage.getItem("id");
		if(idAdmin === null){
			idAdmin = Random.id();
			localStorage.setItem("id", idAdmin);
		}
		//Save the id admin in the webrowser
		const target = event.target;

		const name = target.name.value;
		const privateValue = target.private.checked;
		const role = document.getElementsByClassName("radio-button-selected")[0].attributes.value.value;

		Meteor.call('rooms.insert', name, privateValue, role, idAdmin, function(error,result) {
			window.location = "/role/" +  result;
		});
	},
});

