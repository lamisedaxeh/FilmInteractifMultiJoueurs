//Chat main script
import { Rooms } from '../../../api/rooms.js'; import { Template } from
'meteor/templating';


Template.registerHelper('dateFromNow', (date) => {
  return moment(date).fromNow();
});

import './chat.html';

Template.chat.onCreated(function (){
    var template = this;
    //Sub on template, if template was remove -> sub was remove to
    template.autorun(function (){
        const id = FlowRouter.getParam('id'); template.subscribe('oneRoom', id);
    });
});

//Get all MSG
Template.messagesList.helpers({
  messages: function(){
    const id = FlowRouter.getParam('id');
    
    return Rooms.findOne(id).messages;
  }, roomName(){
    const id = FlowRouter.getParam('id'); room = Rooms.findOne( { _id : id } );
    return room.name;
  },
});

Template.messagesList.events({
  //Send MSG
  'submit .sendMsg'(event){
    const id = FlowRouter.getParam('id'); event.preventDefault();

    const target = event.target; const message = target.text.value; const user =
    localStorage.getItem("pseudo");

    // insert MSG to DB
    if(message.trim() != ""){
      Meteor.call('rooms.insertMessage', id, user, message, {function(err,num){
        console.log("hy" + err + "-" + num);
      }});
    }

    target.text.value = '';
  },
});


 