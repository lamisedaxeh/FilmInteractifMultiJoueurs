import { Rooms } from '../../../api/rooms.js';
import './joinRoom.html';

Template.roomsList.onCreated(function roomsListOnCreated(){
    var template = this;
    template.autorun(function (){
		template.subscribe('allRooms');
	});
});


Template.roomListRow.onRendered(function () {

});



//Get all rooms
Template.roomsList.helpers({
	roomsList: function(){
		return Rooms.find({"isPrivate" : false});
	},
});

Template.roomListRow.helpers({
    isLibre: function(choixRole){
        if(choixRole==="libre")
            return true;
    },
    isAleatoire: function(choixRole){       
        if(choixRole==="aleatoire")
            return true;
    },
    isProprietaire: function(choixRole){
        if(choixRole==="proprietaire")
            return true;
    },
});


Template.joinRoom.events({
	'submit .joinRoom'(event){
		event.preventDefault();

		const target = event.target;

		const num = target.num.value;

		window.location = "/role/"+num;
	},
});

