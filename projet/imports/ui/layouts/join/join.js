import './join.html';
import '../../components/createRoom/createRoom.js';
import '../../components/joinRoom/joinRoom.js';

Template.join.helpers({
		//get pseudo
		pseudo(){
			return localStorage.getItem("pseudo");
		},
});



Template.join.onRendered(function () {
});

Template.join.events({
		'click #btnjoin': function () {
			// click to display join
			document.getElementById("create").classList.add("hide"); 
			document.getElementById("join").classList.remove("hide"); 
			document.getElementById("btnjoin").classList.add("hide"); 
			document.getElementById("btncreate").classList.remove("hide"); 
		},
		'click #btncreate': function () {
			// click to display create
			document.getElementById("join").classList.add("hide"); 
			document.getElementById("create").classList.remove("hide"); 
			document.getElementById("btncreate").classList.add("hide"); 
			document.getElementById("btnjoin").classList.remove("hide"); 
		},
});
