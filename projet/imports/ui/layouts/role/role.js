import './role.html';
import '../../style/style.css';

Template.role.onCreated(function (){
	var template = this;
	//Sub on template, if template was remove -> sub was remove to
	template.autorun(function (){
		const id = FlowRouter.getParam('id');
		template.subscribe('oneRoom', id);
	});
});


Template.role.helpers({
/*	jouePar(){
		const id = FlowRouter.getParam('id');
		
		var room = Rooms.findOne( { _id : id } );
		var result;
		console.log("jouer par");
		for (i in room.users){
			result.push(room.users[i]);
		}
		console.log(result);
		return result;
	},*/
});
	

Template.role.events({
    'click .role1': function(){
		const roomId = FlowRouter.getParam('id');
		var userName = localStorage.getItem("pseudo");
		var userId = localStorage.getItem("id");
		var userRole = "r1";
		localStorage.setItem(roomId, userRole);
		Meteor.call('rooms.insertPlayerAndRole', roomId, userId, userName,userRole);
		window.location = "/ownerRoom/" + roomId;
    },
    'click .role2': function(){
		const roomId = FlowRouter.getParam('id');
		var userName = localStorage.getItem("pseudo");
		var userId = localStorage.getItem("id");
		var userRole = "r2";
		localStorage.setItem(roomId, userRole);
		Meteor.call('rooms.insertPlayerAndRole', roomId, userId, userName,userRole);
		window.location = "/ownerRoom/" + roomId;
    },
	'click .role3': function(){
		const roomId = FlowRouter.getParam('id');
		var userName = localStorage.getItem("pseudo");
		var userId = localStorage.getItem("id");
		var userRole = "r3";
		localStorage.setItem(roomId, userRole);
		Meteor.call('rooms.insertPlayerAndRole', roomId, userId, userName,userRole);
		window.location = "/ownerRoom/" + roomId;
    },
	'click .role4': function(){
		const roomId = FlowRouter.getParam('id');
		var userName = localStorage.getItem("pseudo");
		var userId = localStorage.getItem("id");
		var userRole = "r4";
		localStorage.setItem(roomId, userRole);
		Meteor.call('rooms.insertPlayerAndRole', roomId, userId, userName,userRole);
		window.location = "/ownerRoom/" + roomId;
    },
/*
 'click .carousel-item': function (event) {
	
	var modal = document.getElementById("myPopup");
	modal.style.display = "block";
	},
 'click #no': function (event) {	
	var modal = document.getElementById("myPopup");
	modal.style.display = "none";
 },

 'click #yes': function (event) {	
	var modal = document.getElementById("myPopup");
	modal.style.display = "none";
	var x = document.getElementById(event.currentTarget.id);
	x.disabled = true;
	x.style.filter= "grayscale(100%)";
	var url = window.location.href;
	url=url.split('/').pop()
	window.location="/ownerRoom/"+url;
 },
*/


});



