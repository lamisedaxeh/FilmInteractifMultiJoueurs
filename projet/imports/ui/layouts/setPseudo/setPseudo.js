import { Template } from 'meteor/templating';
import { Random } from 'meteor/random';

import './setPseudo.html';

Template.pseudo.events({
	//Set pseudo
	'submit .pseudo'(event){

		event.preventDefault();
		
		const target = event.target;
		const text = target.text.value;
		
		if(text.trim() !== ""){
			//Set random id
			id = localStorage.getItem("id");
			if(id === null){
				id = Random.id();
				localStorage.setItem("id", id);
			}
			
			//Pseudo save in global session var
			localStorage.setItem("pseudo", text);
			
			window.location = '/join';
		}
	},

});
