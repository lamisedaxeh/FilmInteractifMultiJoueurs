import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

import SimpleSchema from 'simpl-schema';

rooms = new Mongo.Collection('rooms');
/*
Player = new SimpleSchema({
	time:{
		type: Number,
		label: "Time vidéo pas in sec"
	},
	status:{
		type: String,
		label: "Status of vidéo play pause"
	}
});

Messages = new SimpleSchema({
	message:{
		type: String,
		label: "Content of MSG"
	},
	user:{
		type: String,
		label: "Utilisateur"
	},
	created_at:{
		type: Date,
		label: "Created at",
		autoValue: function(){
			return new Date();
		}
	}
});

RoomsSchema = new SimpleSchema({
	name:{
		type: String,
		label: "Name"
	},
	private:{
		type: Boolean,
		label: "Private"
	},
	choixrole:{
		type: String,
		label: "Choix du role"
	},
	user:{
		type: Array,
		label: "List utilisateur"
	},
	'user.$':{
		type: String
	},
	player:{
		type: Player,
		label: "Info about video player"
	},
	messages:{
		type : Array,
		label: "list of message"
	},
	'messages.$':{
		type: Messages
	},
	created_at:{
		type: Date,
		label: "Created at",
		autoValue: function(){
			return new Date();
		}
	}
});

rooms.attachSchema( RoomsSchema );
*/
export const Rooms = rooms;

Meteor.methods({

	'rooms.insert' (name, privateValue, role, idAdmin ){
		check(name,String);
		check(role,String);
		check(privateValue,Boolean);
		check(idAdmin,String);

		return Rooms.insert({
			name: name,
			isPrivate: privateValue,
			choixrole: role,
			idAdmin: idAdmin,
			node: 0,
			createdAt: new Date()
		});
	},
	'rooms.insertPlayerAndRole'(roomId, userId, userName, role){
	    check(roomId, String);
	    check(userId, String);
	    check(userName, String);
	    check(role, String);

		
		if( Rooms.find({"_id": roomId,"users._id": userId}).count() === 0 ){
			Rooms.update(
				roomId ,
				{
					$push: {
						users:{
							_id: userId,
							userName: userName,
							role: role,
						}
					}
				}
			);
		}else{
			Rooms.update(
				{
					"_id": roomId,
					"users._id": userId
				},
				{
					$set: {
						"users.$.userName": userName,
						"users.$.role": role,
					}
				}
			);	
		}		
	},
	
	'rooms.insertChoix'(roomId, userId, userName, role, choix, actualNode){
	    check(roomId, String);
	    check(userId, String);
	    check(userName, String);
	    check(role, String);

		
		if( Rooms.find({"_id": roomId,"users._id": userId}).count() === 0 ){
			Rooms.update(
				roomId ,
				{
					$push: {
						users:{
							_id: userId,
							userName: userName,
							role: role,
							choix: {
								choix: choix,
								actualNode : actualNode
							}
						}
					}
				}
			);
		}else{
			Rooms.update(
				{
					"_id": roomId,
					"users._id": userId
				},
				{
					$set: {
						"users.$.userName": userName,
						"users.$.role": role,
						"users.$.choix": {
							choix: choix,
							actualNode : actualNode
						},
					}
				}
			);	
		}		
	},
	'rooms.insertMessage'(roomId, user, message){
		check(roomId, String);
		check(user, String);
		check(message, String);

		Rooms.update( roomId ,{ $addToSet: { messages:{
			user: user,
			message: message,
			createdAt: new Date(),
		}}});
	},
	
	'rooms.setStatusPlaying'(roomId, playing) {
		check(roomId, String);
		check(playing, Boolean);

		Rooms.update({_id:roomId}, { $set: { playing: playing } });
	},
	
	'rooms.setNode'(roomId, node) {
		check(roomId, String);
		check(node, String);

		Rooms.update({_id:roomId}, { $set: { node: node } });
	},
	
	'rooms.setStatusTimePlayer'(roomId, timePlay) {
		check(roomId, String);
		check(timePlay, Number);

		Rooms.update({_id:roomId}, { $set: { currentTime: timePlay } });
	},

	/*'rooms.remove'(roomId){
	check(roomId, String);
	Rooms.remove(roomId)
    },
    'tasks.setChecked'(taskId, setChecked) {
	check(taskId, String);
	check(setChecked, Boolean);

	Tasks.update(taskId, { $set: { checked: setChecked } });
    },*/
});

if (Meteor.isServer){
	Meteor.publish('allRooms', function allRooms(){
		return Rooms.find();
	});
	Meteor.publish('oneRoom', function roomMessages(roomId){
		check(roomId, String);
		return Rooms.find({_id:roomId});
	});
}
