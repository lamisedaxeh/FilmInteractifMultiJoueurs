[//]: # (Ceci est un readme en markdown il est conseillé de le lire directement sur la page du git : https://gitlab.com/lamisedaxeh/FilmInteractifMultiJoueurs)
[//]: # (Ceci est un readme en markdown il est conseillé de le lire directement sur la page du git : https://gitlab.com/lamisedaxeh/FilmInteractifMultiJoueurs)
[//]: # (Ceci est un readme en markdown il est conseillé de le lire directement sur la page du git : https://gitlab.com/lamisedaxeh/FilmInteractifMultiJoueurs)
[//]: # (Ceci est un readme en markdown il est conseillé de le lire directement sur la page du git : https://gitlab.com/lamisedaxeh/FilmInteractifMultiJoueurs)
[//]: # (Ceci est un readme en markdown il est conseillé de le lire directement sur la page du git : https://gitlab.com/lamisedaxeh/FilmInteractifMultiJoueurs)


# Contexte du projet : 
Coopération avec un groupe d'étudiants en audio-visuel. L’objectif est de réaliser un «film-jeu» multijoueur.
# INSTALLATION en DEV/TEST MODE :
Le dernier test d'installation a été effectué avec 
* **OS:** Arch Linux x86_64
* **Kernel:** 5.6.12-arch1-1

## Requis pour l'installation 
*  Un ordinateur 64bit 
*  Une connexion Internet pour l'installation des paquets et library.
*  Une connexion Internet pour visionner les vidéos d'exemple.

## Installation de Meteor
### Deux solution pour Linux / OS X : 
* Via le gestionnaire de paquets de votre distribution ([meteor](https://aur.archlinux.org/packages/meteor/) sur le Arch User Repository)
* Via la commande `curl https://install.meteor.com/ | sh`

### Pour Windows 
* Vous devez avoir [chocolatey](https://chocolatey.org/install) installé puis exécuté cette commande en administrateur `choco install meteor`
 
Pour plus d'informations sur l'installation de meteor je vous invite à consulter directement sur la documentation meteor [https://www.meteor.com/install#!](https://www.meteor.com/install#!)

## Installation du projet
Une fois la dernière version de meteor d'installé sur votre ordinateur vous devez récupérer le projet par exemple avec :
* `git clone https://gitlab.com/lamisedaxeh/FilmInteractifMultiJoueurs.git`.

Une fois les fichiers du projet récupérés allez dans le dossier `FilmInteractifMultiJoueurs/projet` ici vous trouverez le code source de l'application.
*  Lancer l'application avec la commande : `meteor`

Meteor va vous lister les bibliothèques NPM manquantes pour lancer l'application correctement.
Meteor doit vous donner les commandes pour installer les bibliothèques manquantes.
*  Installer les paquets avec les commandes donnés par meteor !


L'installation est fini ! 

## Lancement
Pour lancer le projet vous devez aller dans le dossier `FilmInteractifMultiJoueurs/projet` puis lancer l'application avec la commande `meteor`.
Le site web sera donc ouvert en local à cette adresse [http://localhost:3000/](http://localhost:3000/)
