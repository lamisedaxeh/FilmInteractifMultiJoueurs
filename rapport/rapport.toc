\babel@toc {french}{}
\contentsline {section}{\numberline {1}Import du cahier des charges :}{3}{section.1}% 
\contentsline {section}{\numberline {2}Analyse}{22}{section.2}% 
\contentsline {subsection}{\numberline {2.1}Rappel de l'objectif}{22}{subsection.2.1}% 
\contentsline {paragraph}{Les joueurs}{22}{section*.2}% 
\contentsline {paragraph}{Les propriétaires de salon}{22}{section*.3}% 
\contentsline {paragraph}{Les administrateur}{22}{section*.4}% 
\contentsline {subsubsection}{\numberline {2.1.1}Choix de plateforme : }{23}{subsubsection.2.1.1}% 
\contentsline {paragraph}{}{23}{section*.6}% 
\contentsline {subsection}{\numberline {2.2}Base de données}{23}{subsection.2.2}% 
\contentsline {subsection}{\numberline {2.3}Système de base de données}{23}{subsection.2.3}% 
\contentsline {subsection}{\numberline {2.4}Les documents}{24}{subsection.2.4}% 
\contentsline {section}{\numberline {3}Conception}{27}{section.3}% 
\contentsline {subsection}{\numberline {3.1}Les pages}{28}{subsection.3.1}% 
\contentsline {paragraph}{Choix du pseudo}{28}{section*.7}% 
\contentsline {paragraph}{Rejoindre un salon}{28}{section*.8}% 
\contentsline {section}{\numberline {4}Codage}{28}{section.4}% 
\contentsline {subsection}{\numberline {4.1}Les fichiers}{28}{subsection.4.1}% 
\contentsline {paragraph}{}{28}{section*.9}% 
\contentsline {subsection}{\numberline {4.2}Les modules}{28}{subsection.4.2}% 
\contentsline {paragraph}{}{28}{section*.10}% 
\contentsline {paragraph}{CSS}{28}{section*.11}% 
\contentsline {subsection}{\numberline {4.3}Arborescence de fichiers}{29}{subsection.4.3}% 
\contentsline {paragraph}{}{29}{section*.12}% 
\contentsline {subsection}{\numberline {4.4}Gestion de versions}{31}{subsection.4.4}% 
\contentsline {subsection}{\numberline {4.5}Gestion des tâche}{31}{subsection.4.5}% 
\contentsline {section}{\numberline {5}Tests}{31}{section.5}% 
\contentsline {subsection}{\numberline {5.1}Test unitaire et test d'intégration}{31}{subsection.5.1}% 
\contentsline {subsection}{\numberline {5.2}Test fonctionnel}{31}{subsection.5.2}% 
\contentsline {subsection}{\numberline {5.3}Test croisé}{31}{subsection.5.3}% 
\contentsline {subsection}{\numberline {5.4}Les tests et git}{31}{subsection.5.4}% 
\contentsline {section}{\numberline {6}Bilan}{32}{section.6}% 
\contentsline {subsection}{\numberline {6.1}Maintenance du système}{32}{subsection.6.1}% 
\contentsline {section}{\numberline {7}Conclusion}{32}{section.7}% 
